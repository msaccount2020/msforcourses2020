package task4;
import java.util.Scanner;

public class TextAnalizer {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("text: ");
        String text = scanner.nextLine();
        System.out.print("word: ");
        String word = scanner.nextLine();

        TextAnalizer analizer = new TextAnalizer(text);
        System.out.printf("\"%s\" count: %d \n", word, analizer.wordCount(word));
    }

    private String text;
    TextAnalizer(String text) {
        this.text = text;
    }

    // сравнение символов с разным регистром
    private boolean equalChar(char left, char right) {
        if(left >= 'a' && left <= 'z') {
            left = (char)((int)left - 32);
        }
        if(right >= 'a' && right <= 'z') {
            right = (char)((int)right - 32);
        }
        return left == right;
    }
    // текстовый символ
    private boolean tryChar(char ch) {
        return (ch >= 'a' && ch <= 'z')
                || (ch >= 'A' && ch <= 'Z')
                || (ch >= '0' && ch <= '9')
                || ch == '_';
    }

    int wordCount(String word) {
        if(text.length() == 0) return 0;
        int counter = 0, cur_index = 0;
        int len_text = text.length(), len_word = word.length();
        int char_compared;
        while(cur_index < len_text) {
            // Выходим на правильный символ
            while(cur_index < len_text) {
                if(tryChar(text.charAt(cur_index))) break;
                cur_index++;
            }
            if(cur_index == len_text) return counter;
            // сравниваем
            char_compared = 0;
            while(cur_index < len_text
                    && char_compared < len_word) {
                if(!equalChar(text.charAt(cur_index), word.charAt(char_compared)))
                    break;
                cur_index++;
                char_compared++;
            }
            if(cur_index == len_text) {
                if(char_compared == len_word) counter++;
                return counter;
            }
            // Если сравнили по всем символам
            if(char_compared == len_word) {
                if(!tryChar(text.charAt(cur_index))) {
                    counter++;
                }
            }
            while( cur_index < len_text && tryChar(text.charAt(cur_index)) ) cur_index++;
        }
        return counter;
    }
}
