package task6;

import java.util.LinkedList;
import java.util.Scanner;

public class Backpack {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("backpack capacity: ");
        double capacity = scanner.nextDouble();
        Backpack backpack = new Backpack(capacity);
        LinkedList<Thing> things = new LinkedList<Thing>();

        System.out.println("things count: ");
        int things_count = scanner.nextInt();
        System.out.println("things - ");
        for(int i = 0; i < things_count; i++) {
            System.out.printf("thing %d (weight/price): ", i);
            double weight = scanner.nextDouble();
            double price = scanner.nextDouble();
            things.add(new Thing(weight, price));
        }

        backpack.computeMaxPrice(things);
        System.out.printf("max price %f \n", backpack.getMaxPrice());
        var bckpack = backpack.getThings();
        System.out.printf("In backpack - \n");
        for(var thing : bckpack) {
            System.out.printf("thing (weight: %f / price: %f)\n", thing.weight, thing.price);
        }
    }

    static class Thing {
        private double weight;
        private double price;
        Thing() {
            weight = 0.0;
            price = 0.0;
        }
        Thing(double weight, double price) {
            this.weight = weight;
            this.price = price;
        }
        double getWeight() {
            return weight;
        }
        double getPrice() {
            return price;
        }
    }

    private double capacity;    // вместимость рюкзака
    Backpack(double capacity) {
        this.capacity = capacity;
    }

    private LinkedList<Thing> max_things = null;      // Текущий набор рюкзака
    private double max_price = 0;               // Текущая ценность рюкзака
    private void maxPrice(double cur_price, double cur_weight, LinkedList<Thing> inBP, LinkedList<Thing> balance) {
        if(balance.isEmpty()) return;
        var clone_balance = (LinkedList<Thing>) balance.clone();
        for(var value : clone_balance) {
            if(cur_weight + value.weight > capacity) {
                if(cur_price > max_price) {
                    max_price = cur_price;
                    max_things = (LinkedList<Thing>)inBP.clone();
                }
            } else {
                balance.remove(value);
                inBP.add(value);
                maxPrice(cur_price + value.price, cur_weight + value.weight,
                        inBP, balance);
                balance.add(value);
                inBP.remove(value);
            }
        }
    }

    public LinkedList<Thing> getThings() {
        return max_things;
    }
    public double getMaxPrice() {
        return max_price;
    }
    public void computeMaxPrice(LinkedList<Thing> things) {
        max_things = new LinkedList<Thing>();
        max_price = 0.0;
        var balance = (LinkedList<Thing>) things.clone();
        maxPrice(0.0, 0.0, new LinkedList<Thing>(), balance);
    }


}
