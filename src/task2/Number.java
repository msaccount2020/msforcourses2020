package task2;
import java.util.Scanner;

class PairNumber {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("first number: ");
        if(!scanner.hasNextInt()) {
            System.out.println("Error! Input number is not integer!");
            return;
        }
        int firstNumber = scanner.nextInt();
        System.out.print("second number: ");
        if(!scanner.hasNextInt()) {
            System.out.println("Error! Input number is not integer!");
            return;
        }
        int secondNumber = scanner.nextInt();
        PairNumber number = new PairNumber(firstNumber);
        System.out.printf("Greatest common devisor: %d \n", number.getGCD(secondNumber));
        System.out.printf("Least common multiple: %d \n", number.getLCM(secondNumber));
    }

    private int value;
    public PairNumber(int value) {
        this.value = value;
    }

    // Greatest Common Devisor
    int getGCD(int secondValue) {
        int firstValue = Math.abs(value);
        secondValue = Math.abs(secondValue);
        int currentGCD = 1;
        while(firstValue != 0 && secondValue != 0) {
            if(firstValue == secondValue) {
                currentGCD *= firstValue;
                break;
            } else if(firstValue == 0) {
                currentGCD *= secondValue;
                break;
            } else if(secondValue == 0) {
                currentGCD *= firstValue;
                break;
            } else if(firstValue == 1 || secondValue == 1) {
                break;
            } else {
                boolean firstEven = (firstValue & 1) == 0;
                boolean secondEven = (secondValue & 1) == 0;
                if(firstEven && secondEven) {
                    currentGCD *= 2;
                    firstValue >>= 1;
                    secondValue >>= 1;
                    firstEven = (firstValue & 1) == 0;
                    secondEven = (secondValue & 1) == 0;
                } else if(firstEven && !secondEven) {
                    firstValue >>= 1;
                    firstEven = (firstValue & 1) == 0;
                } else if(!firstEven && secondEven) {
                    secondValue >>= 1;
                    secondEven = (secondValue & 1) == 0;
                } else {
                    if(firstValue > secondValue) {
                        firstValue = (firstValue - secondValue) / 2;
                    } else {
                        secondValue = (secondValue - firstValue) / 2;
                    }
                }
            }

        }
        return currentGCD;
    }

    // Least Common Multiple
    int getLCM(int secondValue) {
        return Math.abs(secondValue * value) / getGCD(secondValue);
    }
}
