package task1;
import java.util.Scanner;

public class Number {
    public static void main(String argv[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("number: ");
        if(scanner.hasNextInt()) {
            int num = scanner.nextInt();
            Number number = new Number(num);
            System.out.println("even: " + (number.isEven() ? "yes" : "no") +
                    "; prime: " + (number.isPrime() ? "yes" : "no"));
        } else {
            scanner.next();
            System.out.println("Error! Input number is not integer!");
        }
    }

    private int value;
    public Number(int value) {
        this.value = value;
    }
    // решето Эратосфена
    private boolean[] eratosthenesSieve() {
        if(value == 0) return null;
        int value__ = Math.abs(value);
        int size = (int) Math.sqrt(value__) + 1;
        boolean sieve[] = new boolean[size];
        sieve[0] = sieve[1] = true;
        int divisor = 2, offset;
        while(divisor < size) {
            while(divisor < size && sieve[divisor]) divisor++;
            if(divisor >= size) break;
            if(value__ % divisor != 0) sieve[divisor] = true;
            offset = divisor + divisor;
            while(offset < size) {
                sieve[offset] = true;
                offset += divisor;
            }
            divisor++;
        }
        return sieve;
    }
    // проверка на чётность
    public boolean isEven() {
        return (value & 1) == 0;
    }
    // проверка на простоту
    public boolean isPrime() {
        var sieve = eratosthenesSieve();
        for(int i = 2; i < sieve.length; i++) {
            if(!sieve[i]) return false;
        }
        return true;
    }

}
