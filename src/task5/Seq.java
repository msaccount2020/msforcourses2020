package task5;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Seq {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("size: ");
        int size_seq = scanner.nextInt();
        Seq seq = new Seq();
        System.out.println("sequence: ");
        for(int i = 0;i < size_seq; i++) {
            seq.add(scanner.next());
        }
        var pls = seq.getPolindroms();
        System.out.print("polindroms: ");
        for(var value: pls) {
            System.out.println(value);
        }
    }

    private List<String> seq;
    {
        seq = new LinkedList<String>();
    }
    public void add(String value) {
        seq.add(value);
    }
    List<String> getPolindroms() {
        List<String> polindroms = new LinkedList<String>();
        int left, right;
        for(var value : seq) {
            left = 0;
            right = value.length() - 1;
            while(left <= right && value.charAt(left) == value.charAt(right)) {
                left++;
                right--;
            }
            if(left > right) {
                polindroms.add(value);
            }
        }
        return polindroms;
    }
}
