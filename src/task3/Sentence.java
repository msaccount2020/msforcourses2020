package task3;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

public class Sentence {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("text: ");
        String text = scanner.nextLine();
        Sentence sentence = new Sentence(text);
        System.out.printf("count of words: %d; \n words: ", sentence.wordCount());
        var words = sentence.sortWords();
        for(var word : words) {
            System.out.printf("\"%s\" ", word);
        }
        System.out.println();
    }

    private String text;
    Sentence(String text) {
        this.text = text;
    }

    int wordCount() {
        return text.split(" ").length;
    }

    String[] sortWords() {
        var words = text.split(" ");
        for (int i = 0; i < words.length; i++){
            char ch = words[i].charAt(0);
            if(ch >= 'a' && ch <= 'z') {
                String sim = String.format("%c", ch);
                words[i] = words[i].replaceFirst(sim, sim.toUpperCase());
            }
        }
        Arrays.sort(words);
        return words;
    }
}
